"""
This module is intended to analyze respiratory signals and see how many breaths the user takes in a certain
time. For now, let's use number of breaths in 1 minute.

The outcome of this project is intended to build a table result that tells the user their Respiratory Rate
Activity (e.g. Within 1 minute, you take 20 breaths).

The required device is Lithic sensor. The module to run Lithic sensor is lithic.py

Sometimes, the Lithic sensor might not be able to detect number of breaths per minute very accurately. Hence, it
will return an approximate result.

"""

import asyncio
import json
from time import *
from lithic import Lithic
import scipy
from scipy.signal import butter, sosfilt
import sys


async def sprint10(lithic):

    # Output a message informing the user of the appropriate sensor module orientation.
    print("\n******** IMPORTANT INSTRUCTION. PLEASE READ CAREFULLY ********\n")
    print(
        'Please orient your Lithic sensor device so the rounded case of sensor is facing up to your chin, the LED'
        'light pointing away from you, and your Lithic sensor is hold firmly on your chest with your left hand.\n'
        '\nPlease sit up straight and still on your seat, and breathe normally for 1 minute during the recording time.\n'
        '\nEnsure that the sensor has a direct contact with your chest (without clothing layer) and the flat'
        ' side of the sensor is pressed firmly on your chest.\n')

    # Connect to the sensor system
    print('Checking server connection')
    res = await lithic.connectToServer()

    # Quit program if connection is not established
    if res == "Server unreachable":
        print(res)
        raise SystemExit
    else:
        print('OK')
    str(input('Press ENTER to start recording'))
    print('OK')

    # Start recording stream of data
    await lithic.startRecording()
    print('Recording Started\n')
    print('\nDon\'t forget to breathe normally :)')

    while True:
        # Record time=60 seconds.
        sleep(60)

        # Prepare new file and add header
        file = open('data.txt', 'w')  # Opens file for writing, creates file if it does not exist
        file.write("timestamp,acc_x,acc_y,acc_z,gyro_x,gyro_y,gyro_z\n")

        # End recording and ask for recorded data
        res = await lithic.stopRecording()

        # Initialize lists
        signal1 = []
        data = json.loads(res)
        file = open('data.txt', 'a')
        for frame in data:
            for sample in frame['samples']:
                file.write(sample + "\n")
                sample_list = sample.strip().split(',')
                signal1.append(float(sample_list[4]))  # Grab the gyro's x-axis
        file.close()

        # Filter signal 1
        fs = 250
        fc = 20
        order = 10
        sos = butter(order, fc, 'low', False, 'sos', fs)
        signal1_filtered = sosfilt(sos, signal1).tolist()

        # 1 peak above threshold = 1 breath
        threshold = 2
        count_breath = 0  # Count the number of breaths during recording time
        breath_lst = []  # A list that contains all data points above threshold

        # Grab index of data points. We need to group data that classifies as 1 breath.
        for breath in signal1_filtered:
            if breath >= threshold:
                breath_lst.append(breath)

        # Based on my observation of my own respiratory rate, each inhalation has around 350 data points.
        # I will equate 350 data points as 1 breath. This value can vary depends on the person.
        chunk = 350  # A chunk of 350 data points
        breath_cycle = [breath_lst[n:n+chunk] for n in range(0, len(breath_lst), chunk)]
        count_breath = len(breath_cycle)

        # Inform user
        print("During 1 minute, you breathe approximately " + str(count_breath) + " times.")
        user_input = str(input("Would you like to perform another test?\nPress 'n' or 'q' to quit. Press other keys to continue: "))
        if user_input == 'q' or user_input == 'n':
            print("Thank you for taking the test. Goodbye :)")
            quit()


async def main():
    await sprint10(Lithic())

asyncio.run(main())

