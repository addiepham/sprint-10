"""
Allow user to listen to different music types. Each audio file plays for 1 minute.
Later, we will implement this with RespiratoryDetection.py to see how music type influences the user's
breathing pattern.
"""

# Just import stuffs
import time
from playsound import playsound
import winsound
import sys


def metal():
    playsound('Mick Gordon - Inferno.mp3')


def RB():
    playsound('Justin Bieber - Come Around Me.mp3')


def Jazz():
    playsound('321Jazz - Piano Brushin.mp3')


def classic():
    playsound('Yann Tiersen - Comptine Dun Autre Ete.mp3')


def Rap():
    playsound('Eminem - Lose Yourself.mp3')


def music():
    print("\n******** IMPORTANT INSTRUCTION. PLEASE READ CAREFULLY ********\n")
    print("You will be asked to listen to five (05) different audio files.\n"
          "Each audio is a type of music: Metal, R&B, Jazz, Classic, Rap\n"
          "While listening to the audio, place your Lithic sensor to your chest as instructed (for Sprint 11 and 12).\n"
          "After listening to each audio file, there'll be a 30s break to let your breathing pattern turn back to normal.\n"
          "\nThe BEEP sound at the beginning of each audio file means that the audio will be playing soon.\n")

    ask = input("Press 'Y' or 'y' if you want to continue: ")
    if (ask == "Y") or (ask == 'y'):

        # Metal
        time.sleep(1.5)
        winsound.Beep(500, 1500)  # Play sound beep for 500Hz for 1 second, aka. 1000 ms
        print("\nMusic type: Metal\nThis is the 1st audio.\nAudio playing...")
        time.sleep(0.5)  # Sleep for 0.5 second
        metal()
        print("\nAudio stopped. You can put your Lithic sensor down for now. Don't forget to put it back on for the next audio.\nBreak for 30s...")
        time.sleep(30)

        # R&B
        print("\n\nPlease put your Lithic sensor back on as instructed.")
        time.sleep(3.5)
        winsound.Beep(500, 1500)  # Play sound beep for 500Hz for 1 second, aka. 1000 ms
        print("\nMusic type: R&B\nThis is the 2nd audio.\nAudio playing...")
        time.sleep(0.5)  # Sleep for 0.5 second
        RB()
        print("\nAudio stopped. You can put your Lithic sensor down for now. Don't forget to put it back on for the next audio.\nBreak for 30s...")
        time.sleep(30)

        # Jazz
        print("\n\nPlease put your Lithic sensor back on as instructed.")
        time.sleep(3.5)
        winsound.Beep(500, 1500)  # Play sound beep for 500Hz for 1 second, aka. 1000 ms
        print("\nMusic type: Jazz\nThis is the 3rd audio.\nAudio playing...")
        time.sleep(0.5)  # Sleep for 0.5 second
        Jazz()
        print("\nAudio stopped. You can put your Lithic sensor down for now. Don't forget to put it back on for the next audio.\nBreak for 30s...")
        time.sleep(30)

        # Classic
        print("\n\nPlease put your Lithic sensor back on as instructed.")
        time.sleep(3.5)
        winsound.Beep(500, 1500)  # Play sound beep for 500Hz for 1 second, aka. 1000 ms
        print("\nMusic type: Classic\nThis is the 4th audio.\nAudio playing...")
        time.sleep(0.5)  # Sleep for 0.5 second
        classic()
        print("\nAudio stopped. You can put your Lithic sensor down for now. Don't forget to put it back on for the next audio.\nBreak for 30s...")
        time.sleep(30)

        # Rap
        print("\n\nPlease put your Lithic sensor back on as instructed.")
        time.sleep(3.5)
        winsound.Beep(500, 1500)  # Play sound beep for 500Hz for 1 second, aka. 1000 ms
        print("\nMusic type: Rap\nThis is the last audio.\nAudio playing...")
        time.sleep(0.5)  # Sleep for 0.5 second
        Rap()
        print("\nAudio stopped.")

        choice = input("\nAll audios are played. Would you like to listen to them again?\n"
                       "Press Y if you would like to listen to the audios again. Press other keys "
                       "if you want to exit the program: ")
        if choice == "Y":
            music()
        else:
            print("Thank you for listening. Goodbye :)")
            sys.exit()

    else:
        print("Invalid key. Try again.")


music()
